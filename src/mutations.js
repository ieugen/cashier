/*
    List of mutations on state store
*/
export const SET_TX_DATE = 'setTxDate'
export const SET_PAYEE = 'setPayee'
export const ADD_POSTING = 'addPosting'
export const CLEAR_POSTINGS = 'clearPostings'
export const DELETE_POSTING = 'deletePosting'
export const MAIN_TOOLBAR = 'mainToolbar'
export const SET_PAYEES = 'setPayees'
// Selection mode
export const SET_SELECT_MODE = 'setSelectMode'
//
export const SET_TITLE = 'setTitle'
export const SET_TRANSACTION = 'setTransaction'
export const TOGGLE_DRAWER = 'toggleDrawer'
