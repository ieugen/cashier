/*
    Set the application version
*/
import Vue from 'vue'

Vue.prototype.$version = "v2019.12.28.3";
